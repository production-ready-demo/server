# @production-ready-demo/server

## API

| API       | METHOD | Description                                                                              |
|-----------|--------|------------------------------------------------------------------------------------------|
| /         | GET    | Get all links                                                                            |
| /shorten  | POST   | shorten a link body: `{originalUrl: "mylonglink"}`<br /> return a short link as `string` |
| /:shortId | GET    | redirect to the original `originalUrl`                                                   |

### Prerequisites

Ensure you have the following software installed on your machine:

- Node.js (v16.x or higher)
- MongoDB installed or docker installed

### Installation

2. Install the project dependencies.

```bash
npm install
```
using `yarn`
```bash
yarn install
```
### Configuration

Before running the application, make sure to set up the required environment variables. Create a `.env` file in `the root of the project` and configure the following variables:

```bash
# You can copy example.env file to .env
cp example.env .env
# Setup your correct environment variables.
# ....
```
### Running dev mode

#### Setup MongoDb database `Docker only`
```bash
# Make sure you setup your `environment variables` & docker compose installed

# 1- exec `mongo-init.sh` script to setup mongo-init.js
# see https://www.mongodb.com/community/forums/t/credentials-with-docker-compose/255648

chmod +x ./mongo-init.sh
# run script
./mongo-init.sh
# run mongo database using docker compose
docker compose -f docker-compose-dev.yml up database -d
```
# Running the server `dev Mode`
```bash
yarn dev

```

## Build the server

### Create new release

1. Make sure you're in the `main branch` and run one of this two commands. A new Tag will be created.

```bash
### To  Build staging release this is for staging or testing
yarn release:staging

### To Build stable release this is production
yarn release:production
```

2. Push the tag to build the docker image in `circleci`

   ```bash
    # staging tags with prefix staging-{v}
    git push origin staging-{v}
   
    # production tags with prefix production-{v}
    git push origin production-{v}
   
   ```
### Docker
#### Build an image

All images should be tagged `:staging:<app-version>`  for staging release & `:production:<app-version>` for the production release or `production`
#### Example
to build an image we can use `.build-docker-image.sh` script
```bash
# add exec permission
chmod +x ./build-docker-image.sh
# first argument should be production or staging
# staging for the staging version tagged with :staging:<app-version>
# --push is optional  to auto push to the gitlab registry after building the image
./build-docker-image.sh <production | staging> [--push]
```




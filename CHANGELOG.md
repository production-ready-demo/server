# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/production-ready-demo/server/compare/production-0.1.1...production-0.1.2) (2024-02-15)

### [0.1.1](https://gitlab.com/production-ready-demo/server/compare/production-0.1.0...production-0.1.1) (2024-02-14)

## [0.1.0](https://gitlab.com/production-ready-demo/server/compare/production-0.0.17...production-0.1.0) (2024-02-14)

### [0.0.17](https://gitlab.com/production-ready-demo/server/compare/production-0.0.16...production-0.0.17) (2024-02-14)

### [0.0.16](https://gitlab.com/production-ready-demo/server/compare/production-0.0.15...production-0.0.16) (2024-02-14)

### [0.0.15](https://gitlab.com/production-ready-demo/server/compare/production-0.0.14...production-0.0.15) (2024-02-14)

### [0.0.14](https://gitlab.com/production-ready-demo/server/compare/production-0.0.13...production-0.0.14) (2024-02-13)


### Bug Fixes

* import package version ([e08eebc](https://gitlab.com/production-ready-demo/server/commit/e08eebcf727dfc59797556b4ea1436cfdf291b78))

### [0.0.13](https://gitlab.com/production-ready-demo/server/compare/production-0.0.12...production-0.0.13) (2024-02-13)


### Features

* add gracefull shutdown ([e3cf882](https://gitlab.com/production-ready-demo/server/commit/e3cf8823b2a2de1fa2924b4c73b59b3b3abab5b7))
* send server info with reponse ([c01fab5](https://gitlab.com/production-ready-demo/server/commit/c01fab58ac4f827f00b0e62ee9c71748d2d9a686))

### [0.0.12](https://gitlab.com/production-ready-demo/server/compare/production-0.0.11...production-0.0.12) (2024-02-11)


### Features

* add health check ([4e39497](https://gitlab.com/production-ready-demo/server/commit/4e39497492a378766b88b0edb43da9123a971843))

### [0.0.11](https://gitlab.com/production-ready-demo/server/compare/production-0.0.10...production-0.0.11) (2024-02-11)


### Features

* change route prefix to /links ([f7740d6](https://gitlab.com/production-ready-demo/server/commit/f7740d64959dc3b55521469d1c5795a8ae0c8fd6))

### [0.0.10](https://gitlab.com/production-ready-demo/server/compare/production-0.0.9...production-0.0.10) (2024-02-10)

### 0.0.9 (2024-02-10)

### [0.0.8](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.7...staging-0.0.8) (2024-02-10)

### [0.0.7](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.6...staging-0.0.7) (2024-02-10)

### [0.0.6](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.5...staging-0.0.6) (2024-02-10)

### [0.0.5](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.4...staging-0.0.5) (2024-02-10)

### [0.0.4](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.3...staging-0.0.4) (2024-02-10)

### [0.0.3](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.2...staging-0.0.3) (2024-02-10)

### [0.0.2](https://gitlab.com/production-ready-demo/server/compare/staging-0.0.1...staging-0.0.2) (2024-02-10)

### 0.0.1 (2024-02-10)

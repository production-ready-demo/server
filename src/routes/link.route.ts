/**
 * This is an example of a route
 */
import express from 'express';
import { linkController } from '../controllers';

export const linkRouter = express.Router();
linkRouter.post('/shorten', linkController.shorten);

linkRouter.get('/', linkController.getAll);
linkRouter.post('/', linkController.shorten);

linkRouter.get('/:shortId', linkController.redirect);

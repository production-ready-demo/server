import 'dotenv/config';
import 'express-async-errors';
import 'reflect-metadata';

import http from 'http';
import { AddressInfo } from 'net';
import { connectToDatabase, ENV } from './config';
import { createExpressServer } from './server';

Object.keys(ENV).forEach((key) => {
  if (!ENV[key]) {
    console.warn(`missing env variable for ${key}`);
  }
});
// test
// This function bootstraps and starts the configured Express server to begin listening for incoming requests.
async function bootstrap() {
  const connection = await connectToDatabase();
  // Create a new instance of an Express server by calling createExpressServer function.
  const app = createExpressServer();

  // Create an http server instance with the created express application instance and start listening on the specified port/IP address.
  const server = http.createServer(app).listen(ENV.SERVER_PORT, '0.0.0.0', () => {
    const addressInfo = server.address() as AddressInfo;
    console.log(`Server running at http://${addressInfo.address}:${addressInfo.port}`);
  });

  // Set up signal trap handlers to gracefully shutdown the server if any of the specified signals are received.
  const signalsTraps: NodeJS.Signals[] = ['SIGTERM', 'SIGINT', 'SIGUSR2'];

  signalsTraps.forEach((type) => {
    process.on(type, async () => {
      try {
        console.log(`Received ${type}. Starting graceful shutdown...`);

        // Close HTTP server
        await new Promise((resolve, reject) => {
          server.close((err) => {
            if (err) {
              console.error('Error closing HTTP server:', err);
              reject(err);
            } else {
              console.log('HTTP server closed.');
              resolve(undefined);
            }
          });
        });
        // Close database connection
        await connection.close();
        console.log('Disconnected from database.');

        console.log('Graceful shutdown completed.');
        process.exit(0); // Exit with success code
      } catch (error) {
        console.error('Error during graceful shutdown:', error);
        process.exit(1); // Exit with error code
      }
    });
  });
}

bootstrap();

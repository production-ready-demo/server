import { Response } from 'express';
import fs from 'fs';
import mongoose from 'mongoose';
import { ENV } from '../config';
// import process from 'process';
/**
 * The function isUrlValid checks if a given string is a valid URL.
 * @param string - The `string` parameter is the URL string that you want to check for validity.
 * @returns The function isUrlValid returns a boolean value. It returns true if the input string is a
 * valid URL, and false if it is not a valid URL.
 */
export function isUrlValid(string) {
  try {
    new URL(string);
    return true;
  } catch (err) {
    return false;
  }
}

export function isMongoValidationError(error) {
  return error instanceof mongoose.Error.ValidationError;
}

export function getShortLinkUrl(path: string) {
  const url = new URL(path, ENV.SHORT_LINK_BASE_URL);
  return url.toString();
}

export function writeCommonHeader(response: Response) {
  response.setHeader('Access-Control-Expose-Headers', 'Short-Link-Version, Short-Link-PID, Short-Link-Hostname');

  const pkgPath = 'package.json';

  const pkg = JSON.parse(fs.readFileSync(pkgPath, 'utf8'));
  response.setHeader('Short-Link-Version', pkg.version);
  response.setHeader('Short-Link-PID', process.pid);
  response.setHeader('Short-Link-Hostname', process.env.HOSTNAME || 'localhost');
}
